package context.events {
	import flash.events.Event;
	
	public class UpdateColorEvent extends Event {
		static public const UPDATE_COLOR:String = "updateColor";
		
		public var color:uint;
		public function UpdateColorEvent(type:String, color:uint, bubbles:Boolean=false, cancelable:Boolean=false) {
			this.color = color;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event {
			return new UpdateColorEvent(type, color, bubbles, cancelable);
		}
	}
}