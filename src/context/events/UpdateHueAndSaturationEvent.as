package context.events {
	import flash.events.Event;
	
	public class UpdateHueAndSaturationEvent extends Event {
		static public const HUE_AND_SATURATION_UPDATE:String = "hueAndSaturationUpdate";
		
		public var color:Number;
		public function UpdateHueAndSaturationEvent(type:String, color:uint, bubbles:Boolean=false, cancelable:Boolean=false) {
			this.color = color;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event {
			return new UpdateHueAndSaturationEvent(type, color, bubbles, cancelable);
		}
	}
}