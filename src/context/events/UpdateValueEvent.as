package context.events {
	import flash.events.Event;
	
	public class UpdateValueEvent extends Event {
		static public const VALUE_UPDATE:String = "valueUpdate";
		
		public var value:Number;
		public function UpdateValueEvent(type:String, value:Number, bubbles:Boolean=false, cancelable:Boolean=false) {
			this.value = value;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event {
			return new UpdateValueEvent(type, value, bubbles, cancelable);
		}
	}
}