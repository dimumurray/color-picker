package context.events {
	import flash.events.Event;
	
	import model.vo.HSVObject;
	
	public class UpdateHSVEvent extends Event {
		static public const UPDATE_HSV:String = "updateHSV";
		
		public var hsv:HSVObject;
		
		public function UpdateHSVEvent(type:String, hsv:HSVObject, bubbles:Boolean=false, cancelable:Boolean=false) {
			this.hsv = hsv;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event {
			return new UpdateHSVEvent(type, hsv, bubbles, cancelable);
		}
	}
}