package context {
	import context.events.UpdateHSVEvent;
	
	import controller.commands.SetHSVColorCommand;
	
	import flash.display.DisplayObjectContainer;
	
	import model.AppModel;
	
	import org.robotlegs.mvcs.Context;
	
	import view.components.RadialPaletteView;
	import view.components.Swatch;
	import view.components.ValueSlider;
	import view.mediators.ApplicationMediatior;
	import view.mediators.RadialPaletteViewMediator;
	import view.mediators.SwatchMediator;
	import view.mediators.ValueSliderMediator;
	
	public class ColorPickerTestContext extends Context {
		public function ColorPickerTestContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true) {
			super(contextView, autoStartup);
		}
		
		override public function startup():void {
			trace("ColorPickerTestContext::startup() invoked.");
			injector.mapSingleton(AppModel);
			
			mediatorMap.mapView(RadialPaletteView, RadialPaletteViewMediator);
			mediatorMap.mapView(ValueSlider, ValueSliderMediator);
			mediatorMap.mapView(Swatch, SwatchMediator);
			mediatorMap.mapView(ColorPickerTest, ApplicationMediatior);
			
			commandMap.mapEvent(UpdateHSVEvent.UPDATE_HSV, SetHSVColorCommand, UpdateHSVEvent);
		}
	}
}