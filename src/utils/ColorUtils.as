package utils {
	public class ColorUtils {
		static public function RGB2HSV(color:uint):Vector.<Number> {
			var hsv:Vector.<Number> = new <Number>[0, 0, 0];
			var r:Number = ((color >> 16) & 0xFF) / 255;
			var g:Number = ((color >> 8) & 0xFF)/255;
			var b:Number = (color & 0xFF) / 255;
			var max:Number = Math.max(r,g,b);
			var min:Number = Math.min(r,g,b);
			
			if(max != 0) {
				hsv[1] = (max - min) / max;
				if(max == r) hsv[0] = 60 * (g - b)/(max-min);
				else if(max == g) hsv[0] = 60 * (b - r)/(max - min) + 120;
				else hsv[0] = 60 * (r - g)/(max - min) + 240;
				if(hsv[0] < 0) hsv[0] += 360;
			}
			
			hsv[2] = max;
			
			return hsv;
		}
		
		static public function HSV2RGB(h:Number, s:Number, v:Number):uint {
			var k:Number;
			var aa:Number;
			var bb:Number;
			var cc:Number;
			var f:Number;
			var r:Number;
			var g:Number;
			var b:Number;
			
			if ( s <= 0) { 
				r = g = b = v;
			} else {
				if (h == 1) {
					h = 0;
				}
				h *= 6.0
				k = Math.floor(h);
				f = h - k;
				aa = v * (1 - s);
				bb = v * (1 - (s * f));
				cc = v * (1 - (s * (1 - f)));
				switch (k) {
					case 0: r = v;   g = cc;  b = aa; break;
					case 1: r = bb;  g = v;   b = aa; break;
					case 2: r = aa;  g = v;   b = cc; break;
					case 3: r = aa;  g = bb;  b = v; break;
					case 4: r = cc;  g = aa;  b = v; break;
					case 5: r = v;   g = aa;  b = bb; break;
				}
			}
			
			r = Math.round(r*255);
			g = Math.round(g*255);
			b = Math.round(b*255);
			return uint((r << 16) + (g << 8) + b);
		}		
	}
}