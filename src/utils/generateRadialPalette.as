package utils {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.Sprite;

	public function generateRadialPalette(radius:uint):Bitmap {
		var bitmapData:BitmapData = new BitmapData(2 * radius, 2 * radius);
		var angle:Number;
		var distance:Number;
		var saturation:Number;
		
		
		for (var i:int = -radius; i < radius; i++) {
			for (var j:int = -radius; j < radius; j++) {
				angle = Math.atan2(j, i) * 180/Math.PI + ((j < 0)?360:0);
				
				distance = Math.sqrt((i * i) + (j * j));
				
				saturation = distance / radius;
				if(saturation > 1) saturation = 1;
				
				bitmapData.setPixel(i + radius, j + radius, ColorUtils.HSV2RGB(angle/360, saturation, 1));
			}
		}
		
		var circleMask:Sprite = new Sprite()
		circleMask.graphics.beginFill(0x000000);
		circleMask.graphics.drawCircle(radius, radius, radius);
		circleMask.graphics.endFill();
		
		circleMask.cacheAsBitmap = true;
		
		bitmapData.draw(circleMask, null, null, BlendMode.ALPHA, null, true);
		
		return new Bitmap(bitmapData);
	}
}