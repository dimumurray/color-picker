package utils {
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	public function generateBoxFrame(width:uint, height:uint, highlight:uint = 0xFFFFFFF, shadow:uint = 0x747474):Sprite {
		var frame:Sprite = new Sprite();
		
		frame.graphics.lineStyle(1, 0x000000, 0.25, true, "normal", "none", "miter");
		frame.graphics.moveTo(0.5, height);
		frame.graphics.lineTo(0.5, 0.5);
		frame.graphics.lineTo(width - 0.5, 0.5);
		frame.graphics.lineTo(width - 0.5, height);
		
		frame.graphics.lineStyle(1, shadow, 1, true, "normal", "none", "miter");
		frame.graphics.moveTo(-0.5, height + 1);
		frame.graphics.lineTo(-0.5, -0.5);
		frame.graphics.lineTo(width + 1, -0.5);
		
		frame.graphics.lineStyle(1, highlight, 1, true, "normal", "none", "miter");
		frame.graphics.moveTo(0, height + 0.5);
		frame.graphics.lineTo(width + 0.5, height + 0.5);
		frame.graphics.lineTo(width + 0.5, 0);
		
		return frame;
	}
}