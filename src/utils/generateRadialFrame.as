package utils {
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.geom.Matrix;

	public function generateRadialFrame(radius:uint, shadow:uint = 0x747474, highlight:uint = 0xFFFFFF):Sprite {
		var frame:Sprite = new Sprite();
		var originX:uint = radius;
		var originY:uint = radius;
		
		var matrix:Matrix = new Matrix();
		matrix.createGradientBox(2*radius, 2*radius, Math.PI/4);
		
		frame.graphics.lineStyle(1,0,.25, true, "normal", "none", "miter");
		frame.graphics.drawCircle(radius,radius, radius - 0.5);
		
		frame.graphics.lineStyle(1,0,1.0, true, "normal", "none", "miter");
		frame.graphics.lineGradientStyle("linear", [shadow, shadow, highlight, highlight], [1,1,1,1], [0,111,143,255], matrix);
		frame.graphics.drawCircle(radius, radius, radius + 0.5);
		
		return frame;
	}
}