package model {
	import context.events.UpdateColorEvent;
	import context.events.UpdateHueAndSaturationEvent;
	import context.events.UpdateValueEvent;
	
	import model.vo.HSVObject;
	
	import org.robotlegs.mvcs.Actor;
	
	import utils.ColorUtils;

	public class AppModel extends Actor {
		private var _selectedColor:HSVObject;
		
		public function get selectedColorRGB():uint {
			if (!_selectedColor) initializeColor();
			
			return ColorUtils.HSV2RGB(
					_selectedColor.hue, 
					_selectedColor.saturation, 
					_selectedColor.value
			);
			
		}
		
		public function set selectedColor(vo:HSVObject):void {
			if(vo.hue !== undefined) _selectedColor.hue = vo.hue;
			
			if(vo.saturation !== undefined) _selectedColor.saturation = vo.saturation;
			
			if(vo.hue !== undefined || vo.saturation !== undefined) {
				dispatch(new UpdateHueAndSaturationEvent(
						UpdateHueAndSaturationEvent.HUE_AND_SATURATION_UPDATE,
						ColorUtils.HSV2RGB(_selectedColor.hue, _selectedColor.saturation, 1)
					));
			}
			
			if(vo.value !== undefined) {
				_selectedColor.value = vo.value;
				dispatch(new UpdateValueEvent(UpdateValueEvent.VALUE_UPDATE, vo.value));
			}
			
			dispatch(new UpdateColorEvent(UpdateColorEvent.UPDATE_COLOR, selectedColorRGB));
		}
		
		public function get selectedColor():HSVObject {
			var copy:HSVObject = new HSVObject();
			copy.hue = _selectedColor.hue;
			copy.saturation = _selectedColor.saturation;
			copy.value = _selectedColor.value;
			
			return copy;
		}
		
		protected function initializeColor():void {
			var color:HSVObject = new HSVObject();
			color.hue = 0;
			color.saturation = 0;
			color.value = 1;
			
			_selectedColor = color;
		}
		
		public function AppModel(){
			super();
			initializeColor();
		}
	}
}