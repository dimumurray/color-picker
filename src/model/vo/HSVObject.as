package model.vo {
	public class HSVObject {
		public var hue:*;
		public var saturation:*;
		public var value:*;
		
		public function HSVObject() {
		}
	}
}