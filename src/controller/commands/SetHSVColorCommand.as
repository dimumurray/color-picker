package controller.commands {
	import context.events.UpdateHSVEvent;
	
	import model.AppModel;
	
	import org.robotlegs.mvcs.Command;
	
	public class SetHSVColorCommand extends Command {
		[Inject]
		public var event:UpdateHSVEvent;
		
		[Inject]
		public var appModel:AppModel;
		
		override public function execute():void {
			appModel.selectedColor = event.hsv;
		}
	}
}