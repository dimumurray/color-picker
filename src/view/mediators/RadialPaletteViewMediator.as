package view.mediators {
	import context.events.UpdateHSVEvent;
	import context.events.UpdateValueEvent;
	
	import model.vo.HSVObject;
	
	import org.robotlegs.mvcs.Mediator;
	
	import view.components.RadialPaletteView;
	
	public class RadialPaletteViewMediator extends Mediator {
		[Inject]
		public var view:RadialPaletteView;
		
		public function RadialPaletteViewMediator(){
			super();
		}
		
		override public function onRegister():void {
			addContextListener(UpdateValueEvent.VALUE_UPDATE, onValueUpdate);
			view.registerUpdateCallback(updateHueAndSaturation);
		}
		
		private function updateHueAndSaturation(hue:Number, saturation:Number):void {
			var hsv:HSVObject = new HSVObject();
			hsv.hue = hue;
			hsv.saturation = saturation;
			
			dispatch(new UpdateHSVEvent(UpdateHSVEvent.UPDATE_HSV, hsv));
		}
		
		private function onValueUpdate(e:UpdateValueEvent):void {
			view.setPaletteValue(e.value);
		}
	}
}