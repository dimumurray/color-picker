package view.mediators {
	import context.events.UpdateHSVEvent;
	import context.events.UpdateHueAndSaturationEvent;
	import context.events.UpdateValueEvent;
	
	import model.vo.HSVObject;
	
	import org.robotlegs.mvcs.Mediator;
	
	import view.components.ValueSlider;
	
	public class ValueSliderMediator extends Mediator {
		[Inject]
		public var valueSlider:ValueSlider;
		
		public function ValueSliderMediator() {
			super();
		}
		
		override public function onRegister():void {
			addContextListener(UpdateHueAndSaturationEvent.HUE_AND_SATURATION_UPDATE, onHueUpdate);
			valueSlider.registerUpdateCallback(updateValue);
		}
		
		private function updateValue(xPercent:Number, yPercent:Number):void {
			var hsv:HSVObject = new HSVObject();
			hsv.value = 1 - yPercent;
			
			dispatch(new UpdateHSVEvent(UpdateHSVEvent.UPDATE_HSV, hsv));
		}
		
		private function onHueUpdate(e:UpdateHueAndSaturationEvent):void {
			valueSlider.setBaseColor(e.color);
		}
	}
}