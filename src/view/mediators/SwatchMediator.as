package view.mediators {
	import context.events.UpdateColorEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	import view.components.Swatch;
	
	public class SwatchMediator extends Mediator {
		[Inject]
		public var view:Swatch;
		
		override public function onRegister():void {
			addContextListener(UpdateColorEvent.UPDATE_COLOR, onUpdateColor);
		}
		
		public function onUpdateColor(e:UpdateColorEvent):void {
			view.setColor(e.color);
		}
	}
}