package view.mediators {
	import org.robotlegs.mvcs.Mediator;
	
	public class ApplicationMediatior extends Mediator {
		[Inject]
		public var mainView:ColorPickerTest;
		
		override public function onRegister():void {
			mainView.createChildren();
		}
	}
}