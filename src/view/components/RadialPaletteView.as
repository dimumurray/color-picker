package view.components {
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	
	import utils.generateRadialFrame;
	import utils.generateRadialPalette;
	
	public class RadialPaletteView extends Sprite {
		static private var _func:Function = function():void{};
		
		private var palette:Bitmap;
		private var frame:Sprite;
		
		private var icon:Sprite;
		
		private var radius:uint;
		
		private var originX:int;
		private var originY:int;
		
		private var hitRegion:Sprite;
		
		private var updateCallback:Function = _func;
		
		private var hasStageMouseMoveListener:Boolean = false;
		
		public function RadialPaletteView(radius:uint = 64){
			super();
			this.radius = radius;
			
			build();
		}
		
		public function registerUpdateCallback(callback:Function = null):void {
			updateCallback = callback || _func;
		}
		
		private function build():void {
			palette = addChild(generateRadialPalette(radius)) as Bitmap;
			frame = addChild(generateRadialFrame(radius)) as Sprite;
			frame.mouseEnabled = false;
			
			var o:Point = new Point(palette.width * .5, palette.height * .5);
			
			hitRegion = addChild(new Sprite()) as Sprite;
			hitRegion.graphics.beginFill(0x000000, 0);
			hitRegion.graphics.drawCircle(o.x, o.y, radius);
			hitRegion.graphics.endFill();
			
			icon = new CircIcon();
			icon.x = o.x;
			icon.y = o.y;
			
			addChild(icon);
			
			icon.hitArea = hitRegion;
			icon.buttonMode = true;
			icon.addEventListener(MouseEvent.MOUSE_DOWN, onPress);
			
			o = palette.localToGlobal(o);
			originX = o.x;
			originY = o.y;
			
		}
		
		private function onPress(e:MouseEvent):void {
			update();
			if(!hasStageMouseMoveListener){
				hasStageMouseMoveListener = true;
				stage.addEventListener(MouseEvent.MOUSE_MOVE, update);
				stage.addEventListener(MouseEvent.MOUSE_UP, onRelease);
			}
		}
		
		private function update(e:MouseEvent = null):void {
			var point:Point = globalToLocal(new Point(stage.mouseX, stage.mouseY));
			
			var deltaX:int = point.x - originX;
			var deltaY:int = point.y - originY;
			
			var theta:Number = Math.atan2(deltaY, deltaX);
			var magnitude:Number = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
			
			if(Math.ceil(magnitude) >= radius) {
				icon.x = originX + Math.cos(theta) * radius;
				icon.y = originY + Math.sin(theta) * radius;
			} else {
				icon.x = point.x;
				icon.y = point.y;
			}
			
			var hue:Number = (theta * 180/Math.PI + ((deltaY < 0)?360:0))/360;
			var saturation:Number = (magnitude >= radius)?1:(magnitude/radius);
			
			updateCallback(hue, saturation);
		}
		
		private function onRelease(e:MouseEvent):void {
			if(hasStageMouseMoveListener) {
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, update);
				stage.removeEventListener(MouseEvent.MOUSE_UP, onRelease);
				hasStageMouseMoveListener = false;
			}
			update();
		}
		
		public function setPaletteValue(val:Number):void {
			val = (val < 0) ? 0 :
				  (val > 1) ? 1 : val;
			
			var offset:int = (val - 1) * 255;
			
			var colorTransform:ColorTransform = new ColorTransform();
			colorTransform.blueOffset = offset;
			colorTransform.redOffset = offset;
			colorTransform.greenOffset = offset;
			
			palette.transform.colorTransform = colorTransform;
		}
	}
}