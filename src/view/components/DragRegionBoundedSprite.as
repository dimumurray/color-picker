package view.components {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public class DragRegionBoundedSprite extends Sprite {
		private var dragRegion:Rectangle;
		private var isDragging:Boolean = false;
		private var updateCallback:Function;
		
		public function DragRegionBoundedSprite(){
			super();
			
			if(stage) {
				init();
			}else {
				addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		
		private function init(e:Event = null):void {
			if(hasEventListener(Event.ADDED_TO_STAGE)) removeEventListener(Event.ADDED_TO_STAGE, init);
			buttonMode = true;
		}
		
		public function setDragRegion(rect:Rectangle = null):void {
			dragRegion = rect || ((parent)?parent.getRect(parent):null);
		}
		
		public function setUpdateCallback(callback:Function):void {
			updateCallback = callback;
		}
		
		override public function startDrag(lockCenter:Boolean=true, bounds:Rectangle=null):void {
			if (isDragging) return;
			
			super.startDrag(lockCenter, bounds || dragRegion);
			
			isDragging = true;
			update(null);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, update);
		}
		
		override public function stopDrag():void {
			if (!isDragging) return;
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, update);
			
			super.stopDrag();
			
			isDragging = false;
			update(null);
		}
		
		private function update(e:MouseEvent):void {
			updateCallback((x - dragRegion.x)/dragRegion.width, (y - dragRegion.y)/dragRegion.height);
		}
	}
}