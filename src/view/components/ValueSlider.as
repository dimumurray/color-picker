package view.components {
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import utils.ColorUtils;
	import utils.generateBoxFrame;
	
	public class ValueSlider extends Sprite {
		private var base:Sprite;
		private var overlay:Sprite;
		private var frame:Sprite;
		private var icon:DragRegionBoundedSprite;
		
		public function ValueSlider(dimensions:Rectangle, IconClass:Class) {
			super();
			
			build(dimensions, IconClass);
			
			if(stage) {
				onAddedToStage();
			} else {
				addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function build(rect:Rectangle, IconClass:Class):void {
			base = addChild(new Sprite()) as Sprite;
			base.buttonMode = true;
			base.graphics.beginFill(0x000000);	
			base.graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
			base.graphics.endFill();
			base.addEventListener(MouseEvent.MOUSE_DOWN, onBaseClicked);
			
			var baseColorTransform:ColorTransform = new ColorTransform();
			baseColorTransform.color = 0xFFFFFF;
			
			base.transform.colorTransform = baseColorTransform;
			
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(rect.width, rect.height, -Math.PI/2);
			
			overlay = addChild(new Sprite()) as Sprite;
			overlay.graphics.beginGradientFill(GradientType.LINEAR, [0,0], [1,0], [0, 255], matrix);
			overlay.graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
			overlay.graphics.endFill();
			overlay.mouseEnabled = false;
			overlay.mouseChildren = false;
			
			frame = addChild(generateBoxFrame(rect.width, rect.height)) as Sprite;
			frame.mouseEnabled = false;
			
			icon = addChild(new DragRegionBoundedSprite()) as DragRegionBoundedSprite;
			icon.addChild(new IconClass() as Sprite);
			icon.addEventListener(MouseEvent.MOUSE_DOWN, onIconPressed);
			
			var dragRegion:Rectangle = rect.clone();
			dragRegion.width = 0;
			
			icon.setDragRegion(dragRegion);
		}
		
		private function onAddedToStage(e:Event = null):void {
			if(hasEventListener(Event.ADDED_TO_STAGE)) removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stage.addEventListener(MouseEvent.MOUSE_UP, onIconReleased);
		}
		
		private function onBaseClicked(e:MouseEvent):void {
			icon.y = e.localY;
			icon.startDrag();
		}
		
		private function onIconPressed(e:MouseEvent):void {
			icon.startDrag();
		}
		
		private function onIconReleased(e:MouseEvent):void {
			icon.stopDrag();
		}
		
		public function registerUpdateCallback(callback:Function):void {
			icon.setUpdateCallback(callback);
		}
		
		public function setBaseColor(color:uint):void {
			var colorTransform:ColorTransform = base.transform.colorTransform;
			colorTransform.color = color;
			base.transform.colorTransform = colorTransform;
		}
	}
}