package view.components {
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	
	import utils.generateBoxFrame;
	
	public class Swatch extends Sprite {
		private var base:Sprite;
		private var frame:Sprite;
		public function Swatch(width:uint, height:uint) {
			super();
			build(width, height);
		}
		
		private function build(width:uint, height:uint):void {
			base = addChild(new Sprite()) as Sprite;
			
			base.graphics.beginFill(0x000000);
			base.graphics.drawRect(0, 0, width, height);
			base.graphics.endFill(); 
			
			var colorTransform:ColorTransform = new ColorTransform();
			colorTransform.color = 0xFFFFFF;
			base.transform.colorTransform = colorTransform;
			
			frame = addChild(generateBoxFrame(width, height)) as Sprite;			
		}
		
		
		public function setColor(color:uint):void {
			var colorTransform:ColorTransform = transform.colorTransform;
			colorTransform.color = color;
			base.transform.colorTransform = colorTransform;
		}
	}
}