package  {
	import context.ColorPickerTestContext;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Rectangle;
	
	import view.components.RadialPaletteView;
	import view.components.Swatch;
	import view.components.ValueSlider;
	
	[SWF(width="192", height="128", frameRate="60")]
	public class ColorPickerTest extends Sprite {
		public var context:ColorPickerTestContext;
		
		private var palette:RadialPaletteView;
		private var slider:ValueSlider;
		private var swatch:Swatch;
		
		private var frame:Sprite;
		
		public function ColorPickerTest() {
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			context = new ColorPickerTestContext(this);
		}
		
		public function createChildren():void {
			trace("ColorPickerTest::createChildren() invoked");
			
			drawBase();
			createComponents();
		}
		
		private function drawBase():void {
			graphics.beginFill(0xCCCCCC);
			graphics.drawRect(0,0,192,128);
			graphics.endFill();
		}
		
		private function createComponents():void {
			palette = new RadialPaletteView(56);
			palette.x = 8;
			palette.y = 8;
			addChild(palette);
			
			slider = new ValueSlider(new Rectangle(0,0,16,112), SliderArrow);
			slider.x = 128;
			slider.y = 8;
			addChild(slider);
			
			swatch = new Swatch(32, 32);
			swatch.x = 152;
			swatch.y = 8;
			addChild(swatch);
		}		
	}
}